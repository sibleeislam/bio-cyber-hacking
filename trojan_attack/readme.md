# Dataset Generation for experiments
Please use the crawler to download E.coli. plasmid DNA sequences. This module is for generating data using 
trojan based insertion.

## Setup
To install required libraries
```
$ pip install -r requirements.txt
```

## Preparation
To get reads frm the crawled DNA sequences and generate payloads use the following script.
```
$ python prepare_raw_data.py
```
The script will generate one file with host DNA and one file with payload into specified locations.

### Data Generation
Please use generate_dataset.py file for generating trojan insertion based data.
```
$ python generate_dataset.py
```
You have to specify file path of host DNA, file path of payloads and path to where
 you want to save the generated data as the parameters of **generate_and_save_dataset** function.

Please also specify the options values as follows:
```
    c: Clean or non trojan file.
    r: Random trojan insertion.
    bs: Best trojan based straight forward insertion.
    ws: Worst trojan based straight forward insertion.
    exs: Save the best, worst and mean scores from straight forward insertion.
    bg: Best trojan based greedy insertion.
    wg: Worst trojan based greedy insertion.
    exbg: Save the best and mean scores from greedy insertion.
    exwg: Save the worst and mean scores from greedy insertion.
 ```

### Prediction using CNN
The CNN script is running in a docker container. To install docker run the following command.
```
$ sudo apt install docker.io
```
If you do not want to use sudo in front of docker command then edit **/etc/hosts** file and add the following line:
```
127.0.1.1 <your machine name>
```
Now run the following commands:
```
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
```
To build the docker image run the following command:
```
$ docker build . -t cnn
```
Sample of running prediction using CNN script in a docker container. 
```
$ docker run -d --name cnn_1_0_random -v /home/ubuntu/trojan_attack:/app cnn --slice_size=1 --dataset_num=0 --scenario=random
```