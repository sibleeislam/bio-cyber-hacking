from trojan_insertion_new.encode_decode import EncodeDecode
import random

class TrojanUtility():
    def __init__(self, fragment_size, start_tag='ACTG', end_tag='ACTG', key=60, retain_pos = 0):
        self.encode_decode = EncodeDecode(start_tag, end_tag, retain_pos)
        self.key = key
        self.fragment_size = fragment_size
        self.retain_pos = retain_pos
        self.start_tag = start_tag
        self.end_tag = end_tag

    def min_required_cover_dna_length(self, msg):
        num_of_fragments = int(len(msg) / self.fragment_size)
        if len(msg)%self.fragment_size != 0:
            num_of_fragments += 1
        nucloetide_for_payload = len(msg) * 4
        order_num_len = 2
        cover_dna_len = nucloetide_for_payload+nucloetide_for_payload*self.retain_pos
        cover_dna_len += (len(self.start_tag)+len(self.start_tag)*self.retain_pos
                          +len(self.end_tag)+len(self.end_tag)*self.retain_pos
                          +order_num_len+order_num_len*self.retain_pos)*num_of_fragments
        return cover_dna_len

    def get_random_cover_dna(self, cover_dna_len):
        cover_dna = random.choices(['A', 'C', 'T', 'G', 'N'], k=cover_dna_len)
        cover_dna = ''.join(cover_dna)
        return cover_dna

    def make_payload(self, msg, cover_dna='', encrypted=False):
        cover_dna_len = self.min_required_cover_dna_length(msg)
        ## for time being
        if len(cover_dna) == 0:
            cover_dna = self.get_random_cover_dna(cover_dna_len)
        ##
        if len(cover_dna) < cover_dna_len:
            print('Error. cover DNA is not enough in lenght to cover the payload')
            exit()

        payload = self.encode_decode.make_payload(msg, self.fragment_size, cover_dna=cover_dna, encrypted=encrypted)
        return payload

    def get_payload(self, input_DNA_string, encrypted=False, key=60):
        return self.encode_decode.get_payload(input_DNA_string, encrypted=encrypted, key=key)

    # convert a shorter payload into actual form (<domain><port><tld> into <domain>.<tld>:<port>)
    def extend_paylaod(self, payload_txt):
        import re
        parts = re.compile("([a-z]+)([0-9]+)([a-z]+)")
        parts = parts.match(payload_txt).groups()
        if len(parts) == 3:
            return '{}.{}:{}'.format(parts[0],parts[2], parts[1])
        else:
            return None



# trojan_utility = TrojanUtility(fragment_size=2, retain_pos=0)
# payload = trojan_utility.make_payload("Hello world", encrypted=True)
# # print(len(payload))
# # payload = trojan_utility.make_payload("Hello world", encrypted=False)
# # print(len(payload))
# payload = trojan_utility.get_payload(payload, encrypted=True)
# print(payload)
payload = 'ATGTCCCCTATACTAGGTTATTGGAAAATTAAGGGCCTTGTGCAACCCACTCGACTTCTTCTGGAATATCTTGAAGAAGAATATGAAGAGCATTTGTATGAGCGCGATTAAGGTGATAAACGGCGATACAAATAGTTTAAATTGGGTTTGGAGTTTCCCAATCTTCCTTATTATATTGATGGTGATGTTAAATTAACACAGTCTATGGCCATCATACGTTATATAGCTGACAAGCACAACATGTTGGGTGGTTGTCCAAAAGAGCGTGCAGAGATTTCAATGCTTGAAGGAGCGGTTTTAGATATCAGATACGGTGTTTCGAGGATTGCTTATAGCAAAGACTTTGAGACTCTCAAAGTAGATTTGCTTAGCAAGCTTCCTGATATGCTAAAAATGTTCGAAGATCGTTTATGTCATAAAACATATTTAAATGGTGATCATGTAACCCATCCTGACTTCATGTTGTATGACGCTCTTGATGTTGTTATATACCTGGACCCAATGTGCCTGGATGCGTTCCCATAATTAATTTGTGTTAAAGAACGTATTGAAGCTATCCCACAATTTGATTAGTACATGAAATCCAGCAAGTATATAGCATGGCCTTTGCAGGGCTGGCAAGCCACGTTTGGTGGTGGCGACCATCCTCCAAAATCGGATCTGGAAGGTCTGTTCCAGGGGCCGTCCAACAGGTACTGGGTGGGGTTTCGGGCGGCAAAGATGTCCCTGAAGGAGACCACCGTGACTCCGGATAAGCGGAAAGGGCTGGTGTACATTCCGCAGACGGACGTCTCGCGTATTCTCTTCTACTGGAAGGACAGGACGTCCGGGATCGTGGGAGACGCCTTGATCATCTTCCCTGACGACTGTGAGTTCAAGCGGGTGCCGCAGTGCCCCAGCGGGAGGGTCTACGTGCTGGAGTTCAAGGCAGGGTCCAAGCGGCTTTTCTTCTGGATGCAGGAACCCAAGACAGACCAGGATGAGGAGCATTGCCGGAAAGN'
payload = 'AAAACCACCGCTACCAGCGGTGGTTTGTTTGCCGGATCAAGAGCTACCAACTCTTTTTCCGAAGGTAACTGGCTTCAGCAGAGCGCAGATACCAAATACTGTCCTTCTAGTGTAGCCGTAGTTAGGCCACCACTTCAAGAACTCTGTAGCACCGCCTACATACCTCGCTCTGCTAATCCTGTTACCAGTGGCTGCTGCCAGTGGCGATAAGTCGTGTCTTACCGGGTTGGACTCAAGACGATAGTTACCGGATAAGGCGCAGCGGTCGGGCTGAACGGGGGGTTCGTGCACACAGCCCAACTTGGCGCGAACGACCTTCACCGGACTGATATACCGACAGCCTGAGCGATGAGTAAGCGTCACGCGTCCCGCAGGGATAAAGGTGGACAAGTATCCGGTAAGCGGCAGGGTCGGAACAGGAGAGCGCACGAGGGAGCTTCCAGGGGGAAACGCCTGGTATCTTTATAGTCCTGTCGGGTTTCGCCACCTCTGACTTGAGCGTCGATTTATGTGACGCTCGCCAGGGTGGCGGGGCCTATGGAAATACGCCAGCAACGCGGCCTTTTTATGGTTCGTGGCCCTTTGCTGGCCTTTTGCTAACATGTTCTTTCCTGCATTATCCCCTGACTCTGTTGATAAGCGTATTACCGCATTTGAATGAGCGGATACCGCTCGGCGCAGGCGAACCACCGATCGCAGTGAGTCAGTGAGCGAGGAAGCGGAAGTGCCCCTGATGCGGTATTTTCTCCTTACGCATCTGTGCGGTATTTCACACCGCATATGGTGCACTCTCCGTACACTCTGCTCTGATGCCGCATAGTTACGCCAGCATACATTCCGCGATCGCTACGTGGCTGGGCCATGGTTGCGCTCCGACACCCGCCAACACCCGCTGACGCGCCCTGACGAGCTTGCCTGCTCCCGGCTTCCGCGTACAGGCAAGCGGTGACCGTCTCTGGGAGCTGCATATGTCAGAGGTTCTCACCTTCATCTCCGAAACN'
trojan_utility = TrojanUtility(fragment_size = 1, start_tag='ACCTG', end_tag='GCTTA', retain_pos=5)
payload = trojan_utility.get_payload(payload, encrypted=True, key=30)
print(payload)
print(trojan_utility.extend_paylaod(payload))
